import Image from "next/image";
import Display from "./components/Display";
import ProductOverview from "./components/ProductOverview";
import GliderJsCarousel from "./components/GliderJsCarousel";

export default function Home() {
  return (
    <>
    <ProductOverview/>
    <Display/>
    <GliderJsCarousel/>
    </>
  );
}
